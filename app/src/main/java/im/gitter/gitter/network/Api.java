package im.gitter.gitter.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import im.gitter.gitter.LoginData;
import im.gitter.gitter.R;
import im.gitter.gitter.UserAgent;

public class Api {

    private static final int SOCKET_TIMEOUT_MILLIS = 10 * 1000;
    private final String host;
    private final Map<String, String> headers;
    private final RetryPolicy retryPolicy;

    public Api(Context context) {
        this(context, new LoginData(context).getAccessToken());
    }

    public Api(Context context, String accessToken) {
        this.host = context.getResources().getString(R.string.api_host);
        this.headers = new HashMap<>();
        headers.put("User-Agent", UserAgent.get(context));
        headers.put("Accept", "application/json");
        if (accessToken != null) {
            headers.put("Authorization", "Bearer " + accessToken);
        }
        this.retryPolicy = new DefaultRetryPolicy(SOCKET_TIMEOUT_MILLIS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

    public JsonObjectRequest createJsonObjRequest(int method, String uri, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        JsonObjectRequest req = new JsonObjectRequest(method, host + uri, null, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        req.setRetryPolicy(retryPolicy);
        return req;
    }

    public JsonObjectRequest createJsonObjRequest(int method, String uri, JSONObject requestBody, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        JsonObjectRequest req = new JsonObjectRequest(method, host + uri, requestBody, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        req.setRetryPolicy(retryPolicy);
        return req;
    }

    public JsonArrayRequest createJsonArrayRequest(int method, String uri, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        JsonArrayRequest req = new JsonArrayRequest(method, host + uri, null, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        req.setRetryPolicy(retryPolicy);
        return req;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public RetryPolicy getRetryPolicy() {
        return retryPolicy;
    }
}
