package im.gitter.gitter.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Suggestion implements RoomListItem {

    private final String id;
    private final String uri;

    public Suggestion(JSONObject json) throws JSONException {
        /*
           {
             "exists": true,
             "userCount": 1350,
             "id": "53ba932f107e137846ba5c7e",
             "uri": "ruby-vietnam/chat",
             "avatarUrl": "https://avatars1.githubusercontent.com/ruby-vietnam?s=48"
           }
        */
        this.id = json.optString("id");
        this.uri = json.getString("uri");
    }

    public String getId() {
        return id;
    }

    public String getUri() {
        return uri;
    }

    @Override
    public String getAvatarUrl(int size) {
        String owner = uri.split("/")[0];
        return "https://avatars.githubusercontent.com/" + owner + "?s=" + size;
    }

    @Override
    public String getName() {
        return uri;
    }

    @Override
    public boolean hasActivity() {
        return false;
    }

    @Override
    public int getUnreadCount() {
        return 0;
    }

    @Override
    public int getMentionCount() {
        return 0;
    }

    @Override
    public String getRoomId() {
        return getId();
    }
}
