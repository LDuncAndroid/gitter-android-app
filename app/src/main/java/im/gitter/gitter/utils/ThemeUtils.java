package im.gitter.gitter.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatDelegate;

public class ThemeUtils {

    private static final String THEME_MODE = "THEME_MODE";
    private final Context context;

    public ThemeUtils(Context context) {
        this.context = context;
    }

    /**
     * Gets AppCompatDelegate.MODE_NIGHT_NO or AppCompatDelegate.MODE_NIGHT_YES
     */
    public void saveThemeMode(int theme) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putInt(THEME_MODE, theme).apply();
    }

    /**
     * returns AppCompatDelegate.MODE_NIGHT_NO or AppCompatDelegate.MODE_NIGHT_YES,
     * defaults to AppCompatDelegate.MODE_NIGHT_NO
     */
    public int loadThemeMode() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(THEME_MODE, 1);
    }

    public boolean isNightModeEnabled() {
        return loadThemeMode() == AppCompatDelegate.MODE_NIGHT_YES;
    }

}
