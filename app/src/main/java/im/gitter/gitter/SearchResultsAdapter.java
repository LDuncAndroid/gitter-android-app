package im.gitter.gitter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import im.gitter.gitter.models.RoomListItem;
import im.gitter.gitter.network.VolleySingleton;

public class SearchResultsAdapter extends RecyclerView.Adapter<RoomListItemViewHolder> implements PositionClickListener {

    private final ImageLoader imageLoader;
    private final Drawable mentionBadge;
    private final Drawable unreadBadge;
    private final Drawable activityBadge;
    private final int avatarSize;
    private final RoomListItemSelectListener listener;
    private List<RoomListItem> results = new ArrayList<>();

    public SearchResultsAdapter(Context context, RoomListItemSelectListener listener) {
        imageLoader = VolleySingleton.getInstance(context).getImageLoader();
        mentionBadge = ContextCompat.getDrawable(context, R.drawable.mention_badge);
        unreadBadge = ContextCompat.getDrawable(context, R.drawable.unread_badge);
        activityBadge = ContextCompat.getDrawable(context, R.drawable.activity_badge);
        avatarSize = context.getResources().getDimensionPixelSize(R.dimen.avatar_with_badge_avatar_size);
        this.listener = listener;
        setHasStableIds(true);
    }

    @Override
    public RoomListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View roomListItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.avatar_with_badge, parent, false);
        return new RoomListItemViewHolder(roomListItemView, this);
    }

    @Override
    public void onBindViewHolder(RoomListItemViewHolder holder, int position) {
        RoomListItem roomListItem = results.get(position);
        RoomListItemViewHolder.bind(holder, roomListItem, imageLoader, avatarSize, mentionBadge, unreadBadge, activityBadge);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    @Override
    public long getItemId(int position) {
        return results.get(position).getUri().hashCode();
    }

    public void setResults(List<RoomListItem> results) {
        this.results = results;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(int position) {
        listener.onSelect(results.get(position));
    }
}
